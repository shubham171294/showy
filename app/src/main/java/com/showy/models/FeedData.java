/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedData {

    @SerializedName("creation_time")
    @Expose
    private long creationTime;
    @SerializedName("no_of_favourite")
    @Expose
    private long noOfFavourite;
    @SerializedName("no_of_comment")
    @Expose
    private long noOfComment;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("admin_id")
    @Expose
    private long adminId;
    @SerializedName("is_favourite_by_user")
    @Expose
    private boolean isFavouriteByUser;
    @SerializedName("no_of_like")
    @Expose
    private long noOfLike;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("pic_url")
    @Expose
    private String picUrl;
    @SerializedName("colors")
    @Expose
    private String colors;
    @SerializedName("is_like_by_user")
    @Expose
    private boolean isLikeByUser;

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public long getNoOfFavourite() {
        return noOfFavourite;
    }

    public void setNoOfFavourite(long noOfFavourite) {
        this.noOfFavourite = noOfFavourite;
    }

    public long getNoOfComment() {
        return noOfComment;
    }

    public void setNoOfComment(long noOfComment) {
        this.noOfComment = noOfComment;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public long getAdminId() {
        return adminId;
    }

    public void setAdminId(long adminId) {
        this.adminId = adminId;
    }

    public boolean isIsFavouriteByUser() {
        return isFavouriteByUser;
    }

    public void setIsFavouriteByUser(boolean isFavouriteByUser) {
        this.isFavouriteByUser = isFavouriteByUser;
    }

    public long getNoOfLike() {
        return noOfLike;
    }

    public void setNoOfLike(long noOfLike) {
        this.noOfLike = noOfLike;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public boolean isIsLikeByUser() {
        return isLikeByUser;
    }

    public void setIsLikeByUser(boolean isLikeByUser) {
        this.isLikeByUser = isLikeByUser;
    }

}

