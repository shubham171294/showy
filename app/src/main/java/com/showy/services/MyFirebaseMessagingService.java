/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.showy.R;
import com.showy.activities.SplashScreenActivity;
import com.showy.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

/**
 * notifications receiver
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private String message, news_text;
    private SharedPreferences mSharedPreferences;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        mSharedPreferences = Utils.getSharedPrefs(this);
        Map<String, String> map = remoteMessage.getData();

//        Utils.showLog(TAG, map.get("message"));
        message = map.get("message");
        news_text = map.get("text");

        Utils.showLog(TAG, "message:" + message);
        Utils.showLog(TAG, "news_text:" + news_text);
//        Utils.showLog(TAG, "news_type:" + news_type);
//        Utils.showLog(TAG, "category:" + category);
//        Utils.showLog(TAG, "news_id:" + news_id);
//        Utils.showLog(TAG, "news_url:" + news_url);
//        Utils.showLog(TAG, "pic_url:" + pic_url);


        sendNotification(SplashScreenActivity.class);
    }

    private void sendNotification(Class activity) {


        Intent intent = new Intent(this, activity);
//        intent.putExtra(Constants.FROM_NOTIFICATION, true);
//        intent.putExtra("url", news_url);
//        intent.putExtra("id", Long.parseLong(news_id));
//        intent.putExtra("newsType", Integer.parseInt(news_type));
//        intent.putExtra("newsCategory", Integer.parseInt(category));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        try {
            message = URLDecoder.decode(message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icon)
                .setLargeIcon(BitmapFactory.decodeResource(getBaseContext().getResources(), R.mipmap.ic_launcher))
                .setContentTitle(message)
                .setContentText(news_text)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.BigTextStyle inboxStyle = new NotificationCompat.BigTextStyle();
        inboxStyle.bigText(message);

        notificationBuilder.setStyle(inboxStyle);

        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());

    }
}
