/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.showy.R;
import com.showy.adapter.ColorSelectionAdapter;
import com.showy.adapter.FeedAdapter;
import com.showy.databinding.FragmentDesignBinding;
import com.showy.models.Feed;
import com.showy.models.FeedData;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DesignFragment extends Fragment {

    private FeedAdapter mFeedAdapter;
    private FragmentDesignBinding binding;
    private ApiEndPointInterface apiEndPointInterface;
    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private long recordsRequired = 10;
    private long start = 0;
    private long color;
    private ArrayList<FeedData> mArrayList = new ArrayList<FeedData>();
    private int pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private LinearLayoutManager linearLayoutManager;
    private long max_results = 0;
    private Dialog dialog;
    private ArrayList<Integer> colorsList;
    private Call<Feed> feedCall;

    public DesignFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_design, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mContext == null) {
            mContext = view.getContext();
        }
        init();
        setListeners();
        if (start == 0) {
            fetchFeed();
        } else if (start <= max_results) {
            fetchFeed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setListeners() {
        binding.fabFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createColorSelectionDialog();
            }
        });
    }

    private void init() {
        apiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
        mSharedPreferences = Utils.getSharedPrefs(mContext);
        mFeedAdapter = new FeedAdapter(mArrayList, getActivity());
        colorsList = Utils.getColorList();

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        binding.rv.setLayoutManager(linearLayoutManager);
        binding.rv.setAdapter(mFeedAdapter);
        binding.rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if (dy > 0) //check for scroll down
                {
                    binding.fabFilter.setVisibility(View.GONE);
                    binding.fabFilter.animate();
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            if (start == 0) {
                                fetchFeed();
                            } else if (start <= max_results) {
                                fetchFeed();
                            }
                        }
                    }
                } else if (dy < 0) {
                    binding.fabFilter.setVisibility(View.VISIBLE);
                    binding.fabFilter.animate();
                }

            }
        });
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                start = 0;
                fetchFeed();
            }
        });
    }

    private void fetchFeed() {

        color = mSharedPreferences.getInt(Constants.USER_SELECTED_COLOR, 0);
        binding.progressBar.setVisibility(View.VISIBLE);
        feedCall = apiEndPointInterface.getFeed(mSharedPreferences.getLong(Constants.USER_ID, 0),
                recordsRequired, start, color);
        feedCall.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                binding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {

                        start = start + recordsRequired;
                        max_results = response.body().getMaxResult();

                        loading = start < max_results;

                        if (response.body().getData().size() != 0) {
                            if (binding.swipeRefresh.isRefreshing()) {
                                mArrayList.clear();
                                binding.swipeRefresh.setRefreshing(false);
                            }
                            mArrayList.addAll(response.body().getData());
                            mFeedAdapter.notifyDataSetChanged();
                            binding.tvNoData.setVisibility(View.GONE);
                        } else {
                            binding.tvNoData.setVisibility(View.VISIBLE);
                            binding.swipeRefresh.setRefreshing(false);
                        }

                    } else {
                        binding.tvNoData.setVisibility(View.VISIBLE);
                        binding.swipeRefresh.setRefreshing(false);
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getReason());
                        else if (response.body().getMessage() != null && !response.body().getMessage().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getMessage());
                        else
                            Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));

                    }
                }
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                if (mArrayList.isEmpty())
                    binding.tvNoData.setVisibility(View.VISIBLE);

                binding.progressBar.setVisibility(View.GONE);

                if (isAdded())
                    Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));

                binding.swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void createColorSelectionDialog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_color_chooser);
        dialog.show();

        Button allDesigns = (Button) dialog.findViewById(R.id.button_all_designs);
        allDesigns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start = 0;
                color = 0;
                if (dialog != null)
                    dialog.cancel();
                mSharedPreferences.edit().putInt(Constants.USER_SELECTED_COLOR, (int) color).apply();
                mArrayList.clear();
                fetchFeed();
            }
        });

        RecyclerView rv = (RecyclerView) dialog.findViewById(R.id.rv);
        rv.setLayoutManager(new GridLayoutManager(mContext, 4));
        rv.setAdapter(new ColorSelectionAdapter(colorsList, new ColorSelectionAdapter.OnColorSelected() {
            @Override
            public void onColorSelectedListener(int position) {
                if (dialog != null)
                    dialog.cancel();

                switch (position) {
                    case 0:
                        color = Constants.Red;
                        break;
                    case 1:
                        color = Constants.Orange;
                        break;
                    case 2:
                        color = Constants.Yellow;
                        break;
                    case 3:
                        color = Constants.Lightsalmon;
                        break;
                    case 4:
                        color = Constants.Lightcoral;
                        break;
                    case 5:
                        color = Constants.Brown;
                        break;
                    case 6:
                        color = Constants.Maroon;
                        break;
                    case 7:
                        color = Constants.Black;
                        break;
                    case 8:
                        color = Constants.Dark_Grey;
                        break;
                    case 9:
                        color = Constants.Ivory;
                        break;
                    case 10:
                        color = Constants.Forest_Green;
                        break;
                    case 11:
                        color = Constants.Pale_Green;
                        break;
                    case 12:
                        color = Constants.Medium_Aqua_Marine;
                        break;
                    case 13:
                        color = Constants.Medium_Turquoise;
                        break;
                    case 14:
                        color = Constants.Medium_Vilot_Red;
                        break;
                    case 15:
                        color = Constants.Magenta;
                        break;
                    case 16:
                        color = Constants.Violet;
                        break;
                    case 17:
                        color = Constants.Hot_Pink;
                        break;
                    case 18:
                        color = Constants.Pink;
                        break;
                    case 19:
                        color = Constants.Rosy_Brown;
                        break;
                    case 20:
                        color = Constants.Sapphire;
                        break;
                    case 21:
                        color = Constants.Blueviolet;
                        break;
                    case 22:
                        color = Constants.Royal_Blue;
                        break;
                    case 23:
                        color = Constants.Sky_Blue;
                        break;
                    case 24:
                        color = Constants.Lavender;
                        break;
                    default:
                        color = Constants.Red;
                }
                start = 0;
                mSharedPreferences.edit().putInt(Constants.USER_SELECTED_COLOR, (int) color).apply();
                mArrayList.clear();
                fetchFeed();
            }
        }));

    }

    @Override
    public void onDestroyView() {
        feedCall.cancel();
        super.onDestroyView();
    }
}
