/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.showy.R;
import com.showy.databinding.FragmentSharePicBinding;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.io.File;

import static android.app.Activity.RESULT_OK;

/**
 * fragment that lets user share his/her pic to be printed
 */
public class SharePicFragment extends Fragment {


    private FragmentSharePicBinding binding;
    private Context context;
    private Dialog dialog;
    private String captured_image = "";
    private File profilePicFile;
    private String imagePath = "";
    private Uri imageUri;

    public SharePicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_share_pic, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (context == null) {
            context = view.getContext();
        }

        binding.ivDeletePic.setVisibility(View.GONE);
        setListeners();
    }

    private void setListeners() {

        binding.ivUploadedPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askPermission();
            }
        });
        binding.ivDeletePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePath = "";
                binding.ivUploadedPic.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.upload_pic_place_holder));
                binding.ivDeletePic.setVisibility(View.GONE);
            }
        });

        binding.buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(imagePath)) {
                    Utils.showToastMessage(context, "Please choose an image to share");
                } else {
                    Utils.share(context, "Share Pic", "Get your nail arts from Showy", imageUri);
                }
            }
        });
    }

    private void askPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            // perform your work
            initiateEditProfilePicImagePopupWindow();
        } else {
            // request for permission
            boolean isDeniedPreviously = shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (isDeniedPreviously) {
                // show an explanation as to why you need EditUserProfileFragment permission and again request for permission
                // if don't ask again box is checked, and we have again asked for permission it will directly call
                // onRequestPermissionResult with Permission_DENIED result
                new AlertDialog.Builder(context)
                        .setTitle(Constants.STORAGE_PERMISSION)
                        .setMessage(Constants.STORAGE_PERMISSION_RATIONALE)
                        .setCancelable(true)
                        .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Utils.startInstalledAppDetailsActivity(context);
                            }
                        })
                        .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                Utils.showToastMessage(context, getString(R.string.permission_denied));
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            } else {
                // request for permission

                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.GET_PROFILE_PIC_STORAGE_PERMISSION); // shows the dialog

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case Constants.GET_PROFILE_PIC_STORAGE_PERMISSION:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initiateEditProfilePicImagePopupWindow();
                } else {
                    new AlertDialog.Builder(context)
                            .setTitle(Constants.STORAGE_PERMISSION)
                            .setMessage(Constants.STORAGE_PERMISSION_RATIONALE)
                            .setCancelable(false)
                            .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Utils.startInstalledAppDetailsActivity(context);
                                }
                            })
                            .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    Utils.showToastMessage(context, getString(R.string.permission_denied));
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
                break;
            // other 'case' lines to check for other
        }
    }

    private void initiateEditProfilePicImagePopupWindow() {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_media_chooser);
        dialog.show();
        TextView cameraSelect = (TextView) dialog.findViewById(R.id.tv_choose_camera);
        TextView gallerySelect = (TextView) dialog.findViewById(R.id.tv_choose_gallery);

        cameraSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                captured_image = System.currentTimeMillis() + ".jpg";
                File file = new File(Environment.getExternalStorageDirectory(), captured_image);
                captured_image = file.getAbsolutePath();
                Uri outputFileUri = Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                startActivityForResult(cameraIntent, Constants.CAMERA_SELECT_REQUEST_CODE);

            }
        });

        gallerySelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, Constants.GALLERY_SELECT_REQUEST_CODE);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // select profile pic from camera app
        if (requestCode == Constants.CAMERA_SELECT_REQUEST_CODE && resultCode == RESULT_OK) {
            if (dialog != null) {
                dialog.cancel();

                try {
                    String imagePath = Utils.compressImage(captured_image);
                    setPicture(imagePath, binding.ivUploadedPic);
                    imageUri = Uri.parse(imagePath);
                } catch (Exception e) {
                    setPicture(captured_image, binding.ivUploadedPic);
                    imageUri = Uri.parse(captured_image);
                }
            }
        } else {
            if (dialog != null)
                dialog.cancel();
        }

        if (requestCode == Constants.GALLERY_SELECT_REQUEST_CODE && resultCode == RESULT_OK) {
            if (dialog != null) {
                dialog.cancel();
                if (data != null) {

                    Uri selectedUri = data.getData();
                    String pathforcalculatesize = Utils.getRealPathFromURI(selectedUri, context);
                    profilePicFile = new File(pathforcalculatesize);


                    long fileSizeInBytes = profilePicFile.length();
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    if (fileSizeInKB < 1024) {
                        setPicture(pathforcalculatesize, binding.ivUploadedPic);
                        imageUri = Uri.fromFile(profilePicFile);
                    } else {
                        try {
                            String imagePath = Utils.compressImage(profilePicFile.getAbsolutePath());
                            setPicture(imagePath, binding.ivUploadedPic);
                            imageUri = Uri.parse(imagePath);
                        } catch (Exception e) {
                            setPicture(profilePicFile.getAbsolutePath(), binding.ivUploadedPic);
                            imageUri = Uri.fromFile(profilePicFile);
                        }
                    }
                    profilePicFile = null;
                } else {
                    // show error or do nothing
                }
            }
        }
    }

    private void setPicture(String imagePath, ImageView imageView) {
        Glide.with(context).load(imagePath).centerCrop().into(imageView);
        binding.ivDeletePic.setVisibility(View.VISIBLE);
        this.imagePath = imagePath;
    }
}
