package com.showy.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class RobotoRegularButton extends Button {
    public RobotoRegularButton(Context context) {
        super(context);
        setCustomFont(context);
    }

    public RobotoRegularButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public RobotoRegularButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }

    private void setCustomFont(Context context) {
        setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf"));
    }
}
