package com.showy.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class RobotoMediumEditText extends EditText{
    public RobotoMediumEditText(Context context) {
        super(context);
        setCustomFont(context);
    }

    public RobotoMediumEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public RobotoMediumEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }

    private void setCustomFont(Context context) {
        setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf"));
    }
}
