/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.databinding.BindingAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.showy.R;
import com.showy.databinding.RvCommentsSingleItemBinding;
import com.showy.models.CommentDelete;
import com.showy.models.CommentsData;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * adapter to show list of comments
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder> {

    private static final java.lang.String TAG = CommentsAdapter.class.getSimpleName();
    private Context mContext;
    private ArrayList<CommentsData> mCommentsArrayList;
    private SharedPreferences mSharedPreferences;

    public CommentsAdapter(ArrayList<CommentsData> mCommentsArrayList) {
        this.mCommentsArrayList = mCommentsArrayList;
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
            mSharedPreferences = Utils.getSharedPrefs(mContext);
        }
        LayoutInflater inflater = LayoutInflater.from(mContext);
        RvCommentsSingleItemBinding binding = RvCommentsSingleItemBinding.inflate(inflater, parent, false);
        return new CommentsViewHolder(binding);
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(imageView.getContext()).load(url).placeholder(R.drawable.profile_icon).centerCrop().into(imageView);
        } else {
            Glide.with(imageView.getContext()).load(R.drawable.profile_icon).into(imageView);
        }
    }

    @Override
    public void onBindViewHolder(final CommentsViewHolder holder, int position) {
        final CommentsData commentsData = mCommentsArrayList.get(holder.getAdapterPosition());
        holder.bind(commentsData);

        if (commentsData.getUserId() == mSharedPreferences.getLong(Constants.USER_ID, 0)) {
            holder.binding.ivDeleteComment.setVisibility(View.VISIBLE);
            holder.binding.ivDeleteComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createDeleteDialog(holder, commentsData.getId());
                }
            });
        } else {
            holder.binding.ivDeleteComment.setVisibility(View.GONE);
        }

    }

    private void createDeleteDialog(final CommentsViewHolder holder, final long commentId) {
        new AlertDialog.Builder(mContext)
                .setMessage("Sure to delete ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteComment(holder, commentId);
                    }
                })
                .setNeutralButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void deleteComment(final CommentsViewHolder holder, long commentId) {
        Utils.showDialog(mContext, "Deleting...");
        ApiEndPointInterface apiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
        Call<CommentDelete> commentDeleteCall = apiEndPointInterface.deleteMyComment(mSharedPreferences.getLong(Constants.USER_ID, 0), commentId);
        commentDeleteCall.enqueue(new Callback<CommentDelete>() {
            @Override
            public void onResponse(Call<CommentDelete> call, Response<CommentDelete> response) {
                Utils.hideDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {
                        mCommentsArrayList.remove(holder.getAdapterPosition());
                        notifyItemRemoved(holder.getAdapterPosition());
                    } else {
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getReason());
                        else
                            Utils.showToastMessage(mContext, mContext.getString(R.string.some_error_occurred));
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentDelete> call, Throwable t) {
                Utils.hideDialog();
                Utils.showToastMessage(mContext, mContext.getString(R.string.some_error_occurred));
                Utils.showLog(TAG, t.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCommentsArrayList.size();
    }

    class CommentsViewHolder extends RecyclerView.ViewHolder {
        private final RvCommentsSingleItemBinding binding;

        CommentsViewHolder(RvCommentsSingleItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(CommentsData commentsData) {
            binding.setComments(commentsData);
            binding.executePendingBindings();
        }
    }
}
