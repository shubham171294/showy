/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.showy.R;

import java.util.ArrayList;

/**
 * Created by shubham on 26/2/17.
 */

public class ColorSelectionAdapter extends RecyclerView.Adapter<ColorSelectionAdapter.ColorViewHolder> {

    private ArrayList<Integer> colorList;
    private Context mContext;
    private OnColorSelected onColorSelected;

    public ColorSelectionAdapter(ArrayList<Integer> colorList, OnColorSelected onColorSelected) {
        this.colorList = colorList;
        this.onColorSelected = onColorSelected;
    }

    @Override
    public ColorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (mContext == null)
            mContext = parent.getContext();

        View view = LayoutInflater.from(mContext).inflate(R.layout.rv_choose_color_single_item, parent, false);

        return new ColorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ColorViewHolder holder, final int position) {

        holder.colorIv.setImageDrawable(ContextCompat.getDrawable(mContext, colorList.get(position)));
        holder.colorIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onColorSelected.onColorSelectedListener(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return colorList.size();
    }

    public class ColorViewHolder extends RecyclerView.ViewHolder {

        private ImageView colorIv;

        public ColorViewHolder(View itemView) {
            super(itemView);
            colorIv = (ImageView) itemView.findViewById(R.id.iv_color_chooser);
        }
    }

    public interface OnColorSelected {
        void onColorSelectedListener(int position);
    }
}
