/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.showy.R;
import com.showy.activities.CommentsActivity;
import com.showy.activities.FeedDetailActivity;
import com.showy.models.FavouriteData;
import com.showy.models.LikeUnlike;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.DownloadImage;
import com.showy.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * recycler view adapter to show feed
 */

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.FeedViewHolder> {

    private Context mContext;
    private ArrayList<FavouriteData> mFeedArrayList;
    private ApiEndPointInterface mApiEndPointInterface;
    private SharedPreferences mSharedPreferences;
    private long tempPos;
    private Activity activity;

    public FavouriteAdapter(ArrayList<FavouriteData> mFeedArrayList, Activity activity) {
        this.mFeedArrayList = mFeedArrayList;
        this.activity = activity;
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
            mApiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
            mSharedPreferences = Utils.getSharedPrefs(mContext);
        }

        View v = LayoutInflater.from(mContext).inflate(R.layout.rv_feed_single_item, parent, false);

        return new FeedViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final FeedViewHolder holder, final int position) {
        final FavouriteData feed = mFeedArrayList.get(position);
        Glide.with(mContext)
                .load(feed.getPicUrl())
                .placeholder(ContextCompat.getDrawable(mContext, R.drawable.placeholder))
                .centerCrop().
                into(holder.mImage);
        /*holder.numLikes.setText(String.valueOf(feed.getNoOfLike()));
        holder.numComments.setText(String.valueOf(feed.getNoOfComment()));

        holder.num_likes = feed.getNoOfLike();
        if (feed.isIsLikeByUser()) {
            holder.is_liked = true;
            holder.mLike.setImageResource(R.drawable.ic_thumbs_liked);
        } else {
            holder.is_liked = false;
            holder.mLike.setImageResource(R.drawable.ic_thumb_up);
        }
*/
        holder.is_fav = true;
        holder.mBookmark.setImageResource(R.drawable.ic_fav);

       /* holder.mLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likeUnlikePost(holder, holder.mLike, holder.is_liked, feed);
            }
        });*/

        holder.mBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                createDialog(holder, holder.mBookmark, holder.is_fav, feed);
            }
        });

        holder.mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                tempPos = getAdapterPosition();
                askPermission(position, feed.getPicUrl());
            }
        });

        holder.mComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, CommentsActivity.class);
                i.putExtra("feed_id", feed.getId());
                mContext.startActivity(i);
            }
        });

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, FeedDetailActivity.class);
                i.putExtra("feed_id", feed.getId());
                i.putExtra("pic", feed.getPicUrl());
                i.putExtra("product_id", feed.getProductId());
                mContext.startActivity(i);
            }
        });
    }

    private void createDialog(final FeedViewHolder holder, ImageView mBookmark, boolean is_fav, final FavouriteData feed) {
        new AlertDialog.Builder(mContext)
                .setMessage("Do you want to remove this post from favourites ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        bookmarkUnbookmarkPost(holder, holder.mBookmark, holder.is_fav, feed);
                    }
                })
                .setNeutralButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void askPermission(int position, String mediaUrl) {

        int permissionCheck = ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            new DownloadImage(mContext, "name", "description").execute(mediaUrl);

        } else {
            // request for permission
            boolean isDeniedPreviously = ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (isDeniedPreviously) {
                // show an explanation as to why you need this permission and again request for permission
                // if don't ask again box is checked, and we have again asked for permission it will directly call
                // onRequestPermissionResult with Permission_DENIED result
                new AlertDialog.Builder(mContext)
                        .setTitle(Constants.STORAGE_PERMISSION)
                        .setMessage("We need this permission to share this image")
                        .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                Utils.startInstalledAppDetailsActivity(mContext);
                            }
                        })
                        .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                Utils.showToastMessage(activity, Constants.STORAGE_PERMISSION_DECLINED);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.GET_COVER_PIC_STORAGE_PERMISSION); // shows the dialog

            }
        }
    }

    @Override
    public int getItemCount() {
        return mFeedArrayList.size();
    }


    /*private void likeUnlikePost(final FeedViewHolder holder, final ImageView imageLike, final boolean is_liked, final FavouriteData feed) {

        imageLike.setClickable(false);

        Call<LikeUnlike> likeUnlikeCall = mApiEndPointInterface.likeUnlikePost(mSharedPreferences.getLong(Constants.USER_ID, 0), feed.getId());
        likeUnlikeCall.enqueue(new Callback<LikeUnlike>() {
            @Override
            public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {
                        imageLike.setClickable(true);

                        if (is_liked) {
                            imageLike.setImageResource(R.drawable.ic_thumb_up);
                            holder.is_liked = false;
                            holder.num_likes--;
                            feed.setNoOfLike(holder.num_likes);
                            holder.numLikes.setText(String.valueOf(holder.num_likes));
                            feed.setIsLikeByUser(false);
                        } else {
                            imageLike.setImageResource(R.drawable.ic_thumbs_liked);
                            holder.is_liked = true;
                            holder.num_likes++;
                            feed.setNoOfLike(holder.num_likes);
                            holder.numLikes.setText(String.valueOf(holder.num_likes));
                            feed.setIsLikeByUser(true);

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeUnlike> call, Throwable t) {
                imageLike.setClickable(true);
                Utils.showToastMessage(mContext, mContext.getString(R.string.some_error_occurred));
                t.printStackTrace();
            }
        });

    }*/

    private void bookmarkUnbookmarkPost(final FeedViewHolder holder, final ImageView imageLike, final boolean is_liked, final FavouriteData feed) {

        imageLike.setClickable(false);

        Call<LikeUnlike> likeUnlikeCall = mApiEndPointInterface.markFavUnfav(mSharedPreferences.getLong(Constants.USER_ID, 0), feed.getFeedId());
        likeUnlikeCall.enqueue(new Callback<LikeUnlike>() {
            @Override
            public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {
                        imageLike.setClickable(true);

                        if (is_liked) {
                            imageLike.setImageResource(R.drawable.ic_unfavourite);
                            holder.is_fav = false;
                            holder.num_fav--;
                            mFeedArrayList.remove(holder.getAdapterPosition());
                            notifyItemRemoved(holder.getAdapterPosition());
                        } else {
                            imageLike.setImageResource(R.drawable.ic_fav);
                            holder.is_fav = true;
                            holder.num_fav++;

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeUnlike> call, Throwable t) {
                imageLike.setClickable(true);
                Utils.showToastMessage(mContext, mContext.getString(R.string.some_error_occurred));
                t.printStackTrace();
            }
        });

    }

    class FeedViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImage, mLike, mComment, mShare, mBookmark;
        private TextView image_id, numLikes, numComments;
        private long num_likes = 0;
        private boolean is_liked;
        private boolean is_fav;
        private long num_fav;
        private CardView mCardView;

        private FeedViewHolder(View itemView) {
            super(itemView);

            mImage = (ImageView) itemView.findViewById(R.id.image);
            image_id = (TextView) itemView.findViewById(R.id.tv_image_id);
            mLike = (ImageView) itemView.findViewById(R.id.like);
            mLike.setVisibility(View.GONE);
            mComment = (ImageView) itemView.findViewById(R.id.comment);
            mComment.setVisibility(View.GONE);
            mShare = (ImageView) itemView.findViewById(R.id.iv_share);
            mBookmark = (ImageView) itemView.findViewById(R.id.iv_bookmark);
            numLikes = (TextView) itemView.findViewById(R.id.tv_num_likes);
            numLikes.setVisibility(View.GONE);
            numComments = (TextView) itemView.findViewById(R.id.tv_num_comments);
            numComments.setVisibility(View.GONE);
            mCardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}
