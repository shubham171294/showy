/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import com.showy.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class DownloadImage extends AsyncTask<String, Integer, Uri> {


    private Context context;
    private String name, desc;
    private ProgressDialog mProgressDialog;

    public DownloadImage(Context context, String name, String desc) {
        this.context = context;
        this.name = name;
        this.desc = desc;
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setTitle("Downloading image to share...");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        Utils.showDialog(context, context.getString(R.string.loading));
        mProgressDialog.show();
    }

    private Uri connect(String picUrl) {
        /*try {
            URL url = new URL(picUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap shareBitmap = BitmapFactory.decodeStream(input);
            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                shareBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), shareBitmap, System.currentTimeMillis() + ".jpeg", null);
                return Uri.parse(path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
        }
        return null;*/
        Utils.showLog("picUrl", picUrl);
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(picUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();

            File mediaStorageDirForImages = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), context.getString(R.string.app_name));
            if (!mediaStorageDirForImages.exists()) {
                if (!mediaStorageDirForImages.mkdirs()) {
                    Utils.showLog("Directory Creation Err", "Failed Failed");
                    return null;
                }
            }
            String extension = url.toString().substring(url.toString().lastIndexOf('.') + 1, url.toString().length());

            File mediafileCameraOutput = new File(mediaStorageDirForImages.getAbsolutePath() + "/" + System.currentTimeMillis() + "." + extension);

            output = new FileOutputStream(mediafileCameraOutput);


            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);

            }

            return Uri.fromFile(mediafileCameraOutput);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
    }

    @Override
    protected Uri doInBackground(String... params) {
        return connect(params[0]);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        // if we get here, length is known, now set indeterminate to false
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Uri uri) {
        super.onPostExecute(uri);
//        Utils.hideDialog();
        mProgressDialog.cancel();
        if (uri != null)
            Utils.share(context, "Check this out", desc, uri);
        else
            Utils.showToastMessage(context, "Can't share at the moment. Try again later.");
    }
}
