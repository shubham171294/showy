package com.showy.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.showy.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * Utils class
 */

public final class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    private static ProgressDialog pDialog = null;

    public static SharedPreferences getSharedPrefs(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences;
    }

    public static void startInstalledAppDetailsActivity(final Context context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    public static void generateHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.showy",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();

        }
    }

    public static String compressImage(String filePath) {

//        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filePath);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 60, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filePath;

    }

    // TODO: 13/12/16 check for null pointer exception
    public static String getRealPathFromURI(Uri contentURI, Context context) {

        try {
            Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) {
                return contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static InputStream getInputStreamFromURI(Uri contentURI, Context context) {

        InputStream inputStream = null;
        try {
            Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
//            int fileIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            cursor.moveToFirst();
//            fileName = cursor.getString(fileIndex);
            inputStream = context.getContentResolver().openInputStream(contentURI);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Utils.showToastMessage(context, "Some error occurred.");
        }

        return inputStream;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    /*
       requires WRITE_EXTERNAL_STORAGE permission
    */
    public static Uri getUri(Bitmap photo, Context context) {
        try {
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), photo, context.getString(R.string.app_name), null);
            return Uri.parse(path);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Utils.showToastMessage(context, context.getString(R.string.some_error_occurred));
        }
        return null;
    }

    public static Snackbar shortSnackBar(View view, String message, int color) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackView = snackbar.getView();
        TextView tv = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(color);
        snackbar.show();
        return snackbar;
    }

    // for long snackbar
    public static Snackbar longSnackBar(View view, String message, int color) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackView = snackbar.getView();
        TextView tv = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(color);
        snackbar.show();
        return snackbar;
    }

    // for indefinite snackbar
    public static Snackbar indefSnackBar(View view, String message, int color) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        View snackView = snackbar.getView();
        TextView tv = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(color);
        snackbar.show();
        return snackbar;
    }

    public static void showLog(String TAG, String message) {

        Log.e(TAG, "" + message);
    }

    public static void showLog(String message) {

        Log.e(TAG, "" + message);
    }

    public static void showToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static Dialog showDialog(Context context, View view, Boolean cancellable) {
        final Dialog dialog = new Dialog(context);
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // inflate the layout dialog_layout.xml and set it as contentView
        dialog.setCancelable(cancellable);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.show();
        return dialog;
    }

    public static void showDialog(Context context, String msg) {

        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            pDialog.setProgressDrawable(new ColorDrawable(Color.BLUE));
            pDialog.setMessage(msg);
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
        } else {
            pDialog.dismiss();
            pDialog = new ProgressDialog(context);
            pDialog.setProgressDrawable(new ColorDrawable(Color.BLUE));
            pDialog.setMessage(msg);
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
//            showDialog(context, msg);
        }
    }

    public static void hideDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    public static ArrayList<Integer> getColorList() {

        ArrayList<Integer> colorsList = new ArrayList<>();
        colorsList.add(R.drawable.drawable_circle_red);
        colorsList.add(R.drawable.drawable_circle_orange);
        colorsList.add(R.drawable.drawable_circle_yellow);
        colorsList.add(R.drawable.drawable_circle_light_salmon);
        colorsList.add(R.drawable.drawable_circle_light_coral);
        colorsList.add(R.drawable.drawable_circle_brown);
        colorsList.add(R.drawable.drawable_circle_maroon);
        colorsList.add(R.drawable.drawable_circle_black);
        colorsList.add(R.drawable.drawable_circle_dark_grey);
        colorsList.add(R.drawable.drawable_circle_ivory);
        colorsList.add(R.drawable.drawable_circle_forest_green);
        colorsList.add(R.drawable.drawable_circle_pale_green);
        colorsList.add(R.drawable.drawable_circle_medium_aqua_marine);
        colorsList.add(R.drawable.drawable_circle_medium_turquoise);
        colorsList.add(R.drawable.drawable_circle_medium_violet_red);
        colorsList.add(R.drawable.drawable_circle_magenta);
        colorsList.add(R.drawable.drawable_circle_violet);
        colorsList.add(R.drawable.drawable_circle_hot_pink);
        colorsList.add(R.drawable.drawable_circle_pink);
        colorsList.add(R.drawable.drawable_circle_rosy_brown);
        colorsList.add(R.drawable.drawable_circle_sapphire);
        colorsList.add(R.drawable.drawable_circle_blue_violet);
        colorsList.add(R.drawable.drawable_circle_royal_blue);
        colorsList.add(R.drawable.drawable_circle_sky_blue);
        colorsList.add(R.drawable.drawable_circle_lavender);
        return colorsList;
    }

    public static void openBrowser(Context mContext, String url) {

        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://" + url;

            i.setData(Uri.parse(url));
            mContext.startActivity(i);
        } catch (ActivityNotFoundException e) {
            Utils.showToastMessage(mContext, "Browser not installed. Please install a browser and continue.");
            e.printStackTrace();
        }
    }

    public static void share(Context context, String subject, String shareBody, Uri imageUri) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("image/*");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Showy");
//        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody + "\n\n Download our app from " + Html.fromHtml(context.getString(R.string.play_store_link)));
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Latest and trendy designs - only in Showy ! Download our app from " + context.getString(R.string.play_store_link));
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        context.startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.share_via)));
    }

    public static void shareText(Context mContext, String desc) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Latest and trendy designs - only in Showy !\n\nDownload our app from " + mContext.getString(R.string.play_store_link));
        sendIntent.setType("text/plain");
        mContext.startActivity(Intent.createChooser(sendIntent, mContext.getString(R.string.share_via)));
    }

    public static void clearApplicationData(Context context) {

        File cacheDirectory = context.getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {

            String[] fileNames = applicationDirectory.list();

            for (String fileName : fileNames) {

                if (!fileName.equals("lib")) {

                    deleteFile(new File(applicationDirectory, fileName));

                }

            }

        }

    }

    public static boolean deleteFile(File file) {

        boolean deletedAll = true;

        if (file != null) {

            if (file.isDirectory()) {

                String[] children = file.list();

                for (int i = 0; i < children.length; i++) {

                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;

                }

            } else {

                deletedAll = file.delete();

            }

        }

        return deletedAll;

    }
}
