/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.utils;

/**
 * save various constants
 */

public class Constants {

    public static final String PROFILE_PIC = "profile_pic";
    public static final String USER_ID = "user_id";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PHONE_NUMBER = "phone";
    public static final String LAT = "lat";
    public static final String LON = "lon";
    public static final String ADDRESS = "address";

    // request codes
    public static final int INTERESTS_SELECTION_REQUEST_CODE = 1;
    public static final int LOOKING_FOR_SELECTION_REQUEST_CODE = 2;
    public static final int GALLERY_SELECT_REQUEST_CODE = 908;
    public static final int IMAGE_SELECT_REQUEST_CODE = 456;
    public static final int VIDEO_SELECT_REQUEST_CODE = 457;
    public static final int CAMERA_SELECT_REQUEST_CODE = 907;
    public static final int GET_IMAGE_STORAGE_PERMISSION = 429;
    public static final int GET_COVER_PIC_STORAGE_PERMISSION = 786;
    public static final int GET_PROFILE_PIC_STORAGE_PERMISSION = 787;
    public static final int USER_LOC_REQUEST_CODE = 603;
    public static final int GET_VIDEO_STORAGE_PERMISSION = 170;
    public static final int GET_AUDIO_STORAGE_PERMISSION = 458;
    public static final int GET_PDF_STORAGE_PERMISSION = 459;
    public static final int IMAGE_GALLERY_SELECT_REQUEST_CODE = 843;
    public static final int VIDEO_GALLERY_SELECT_REQUEST_CODE = 844;
    public static final int PDF_SELECT_REQUEST_CODE = 845;
    public static final int AUDIO_SELECT_REQUEST_CODE = 846;
    public static final int SPECIALIZING_IN_SELECTION_REQUEST_CODE = 847;

    public static final String IS_LOGIN = "is_login";
    public static final String USER_SELECTED_COLOR = "selected_color"; // color selected in match your nail
    public static final String PROFILE_PIC_BITMAP_FILE = "profile_pic_bitmap_file"; //saves pic in file as a bitmap

    // permission results
    public static String STORAGE_PERMISSION = "Storage Permission";
    public static String CAMERA_PERMISSION = "Camera Permission";
    public static String STORAGE_PERMISSION_RATIONALE = "We need this permission so that you can easily change your picture.";
    //    public static String CAMERA_PERMISSION_RATIONALE = "We need this permission so that you can easily change your profile pic from camera.";
//    public static String CAMERA_PERMISSION_DECLINED = "Camera Permission Declined";
    public static String STORAGE_PERMISSION_DECLINED = "Storage Permission Declined";
    public static String STORAGE_PERMISSION_GRANTED = "Storage Permission Granted";
    public static String CAMERA_PERMISSION_GRANTED = "Camera Permission Granted";

    public static final String FCM_TOKEN = "fcm_token";

    //list of colors
    public static final int Red = 101;
    public static final int Orange = 102;
    public static final int Yellow = 103;
    public static final int Lightsalmon = 104;
    public static final int Lightcoral = 105;
    public static final int Brown = 106;
    public static final int Maroon = 107;
    public static final int Black = 108;
    public static final int Dark_Grey = 109;
    public static final int Ivory = 110;
    public static final int Forest_Green = 111;
    public static final int Pale_Green = 112;
    public static final int Medium_Aqua_Marine = 113;
    public static final int Medium_Turquoise = 114;
    public static final int Medium_Vilot_Red = 115;
    public static final int Magenta = 116;
    public static final int Violet = 117;
    public static final int Hot_Pink = 118;
    public static final int Pink = 119;
    public static final int Rosy_Brown = 120;
    public static final int Sapphire = 121;
    public static final int Blueviolet = 122;
    public static final int Royal_Blue = 123;
    public static final int Sky_Blue = 124;
    public static final int Lavender = 125;


    public static final String FROM_WHERE = "from_where";
    public static final String FROM_LOGIN = "from_login";
    public static final String FROM_SIGNUP = "from_signup";

}
