/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.utils;

import com.showy.models.CommentDelete;
import com.showy.models.Comments;
import com.showy.models.Favourites;
import com.showy.models.Feed;
import com.showy.models.LikeUnlike;
import com.showy.models.Otp;
import com.showy.models.PicUpload;
import com.showy.models.PostComment;
import com.showy.models.User;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * retrofit interface for REST API
 */

public interface ApiEndPointInterface {

    String BASE_URL = "http://35.154.1.26:8080/art/";
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build())
            .build();


    @FormUrlEncoded
    @POST("create/user")
    Call<User> createUser(@Field("user_name") String user_name,
                          @Field("email") String email,
                          @Field("phone") String phone,
                          @Field("user_pic") String user_pic,
                          @Field("address") String address,
                          @Field("lat") double lat,
                          @Field("lon") double lon,
                          @Field("gcm_id") String gcm_id
    );

    @FormUrlEncoded
    @POST("send/phone/otp")
    Call<Otp> getOtp(@Field("phone") String phone);


    @Multipart
    @POST("upload/pic/by/user")
//    Call<PicUpload> postImage(@Part MultipartBody.Part image, @Part("name") RequestBody name);
    Call<PicUpload> postImage(@Part MultipartBody.Part file);

    @GET("get/feeds")
    Call<Feed> getFeed(@Query("user_id") long user_id,
                       @Query("record") long record,
                       @Query("start") long start,
                       @Query("color") long color
    );

    @FormUrlEncoded
    @POST("add/like/in/feed")
    Call<LikeUnlike> likeUnlikePost(@Field("user_id") long user_id,
                                    @Field("feed_id") long feed_id);

    @FormUrlEncoded
    @POST("add/favourite")
    Call<LikeUnlike> markFavUnfav(@Field("user_id") long user_id,
                                  @Field("feed_id") long feed_id);

    @FormUrlEncoded
    @POST("get/user/favourites")
    Call<Favourites> getUserFavouriteFeedPost(@Field("user_id") long user_id,
                                              @Query("record") long record,
                                              @Query("start") long start);

    @FormUrlEncoded
    @POST("logout/user")
    Call<User> logout(@Field("user_id") long user_id);

    @FormUrlEncoded
    @POST("update/user/gcm")
    Call<User> updateFcmToken(@Field("user_id") long user_id,
                              @Field("gcm_id") String gcm_id);

    @GET("get/all/comments/in/feed")
    Call<Comments> getAllCommentsInFeed(@Query("feed_id") long feed_id,
                                        @Query("user_id") long user_id);

    @FormUrlEncoded
    @POST("add/comment/in/feed")
    Call<PostComment> postCommentInFeed(@Field("user_id") long user_id,
                                        @Field("comment") String comment,
                                        @Field("feed_id") long feed_id);

    @FormUrlEncoded
    @POST("delete/comment")
    Call<CommentDelete> deleteMyComment(@Field("user_id") long user_id,
                                        @Field("comment_id") long comment_id);


}
