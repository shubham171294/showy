/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.showy.R;
import com.showy.adapter.FavouriteAdapter;
import com.showy.databinding.ActivityFavouriteBinding;
import com.showy.models.FavouriteData;
import com.showy.models.Favourites;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteActivity extends AppCompatActivity {

    private ActivityFavouriteBinding binding;
    private static final String TAG = FavouriteActivity.class.getSimpleName();
    private FavouriteAdapter mFeedAdapter;
    private ApiEndPointInterface apiEndPointInterface;
    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private long recordsRequired = 10;
    private long start = 0;
    private long color;
    private ArrayList<FavouriteData> mArrayList = new ArrayList<>();
    private int pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private LinearLayoutManager linearLayoutManager;
    private long max_results = 0;
    private Dialog dialog;
    private ArrayList<Integer> colorsList;
    private Call<Favourites> feedCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favourite);
        init();
        if (start == 0) {
            fetchFeed();
        } else if (start <= max_results) {
            fetchFeed();
        }
    }


    private void init() {
        mContext = getApplicationContext();

        getSupportActionBar().setTitle("Favourites");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        apiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
        mSharedPreferences = Utils.getSharedPrefs(mContext);
        mFeedAdapter = new FavouriteAdapter(mArrayList, this);
        colorsList = Utils.getColorList();

        color = mSharedPreferences.getInt(Constants.USER_SELECTED_COLOR, 0);
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        binding.rv.setLayoutManager(linearLayoutManager);
        binding.rv.setAdapter(mFeedAdapter);
        binding.rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            if (start == 0) {
                                fetchFeed();
                            } else if (start <= max_results) {
                                fetchFeed();
                            }
                        }
                    }
                }

            }
        });
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                start = 0;
                fetchFeed();
            }
        });
    }

    private void fetchFeed() {

        binding.progressBar.setVisibility(View.VISIBLE);
        feedCall = apiEndPointInterface.getUserFavouriteFeedPost(mSharedPreferences.getLong(Constants.USER_ID, 0),
                recordsRequired, start);

        feedCall.enqueue(new Callback<Favourites>() {
            @Override
            public void onResponse(Call<Favourites> call, Response<Favourites> response) {
                binding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {

                        start = start + recordsRequired;
                        max_results = response.body().getMaxResult();

                        loading = start < max_results;

                        if (response.body().getData().size() != 0) {
                            if (binding.swipeRefresh.isRefreshing()) {
                                mArrayList.clear();
                                binding.swipeRefresh.setRefreshing(false);
                            }
                            mArrayList.addAll(response.body().getData());
                            mFeedAdapter.notifyDataSetChanged();
                        } else {
                            binding.tvNoData.setVisibility(View.VISIBLE);
                            binding.swipeRefresh.setRefreshing(false);
                        }

                    } else {
                        binding.tvNoData.setVisibility(View.VISIBLE);
                        binding.swipeRefresh.setRefreshing(false);
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getReason());
                        else if (response.body().getMessage() != null && !response.body().getMessage().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getMessage());
                        else
                            Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));

                    }
                }

            }

            @Override
            public void onFailure(Call<Favourites> call, Throwable t) {
                if (mArrayList.isEmpty())
                    binding.tvNoData.setVisibility(View.VISIBLE);

                binding.progressBar.setVisibility(View.GONE);

                binding.swipeRefresh.setRefreshing(false);
                Utils.showLog(TAG, t.getMessage());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
