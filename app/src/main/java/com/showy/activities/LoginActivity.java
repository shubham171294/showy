package com.showy.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.showy.R;
import com.showy.databinding.ActivityLoginBinding;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

public class LoginActivity extends AppCompatActivity {


    private ActivityLoginBinding binding;
    private Context activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        init();
    }

    private void init() {

        activity = getApplicationContext();

        getSupportActionBar().setTitle("Login");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.buttonSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPhoneNumberValid()) {
                    Intent i = new Intent(activity, VerifyOtpActivity.class);
                    i.putExtra("phone", binding.etEnterPhoneNumber.getText().toString().trim());
                    i.putExtra(Constants.FROM_WHERE, Constants.FROM_LOGIN);
                    startActivity(i);
                }
            }
        });
    }

    private boolean isPhoneNumberValid() {
        if (TextUtils.isEmpty(binding.etEnterPhoneNumber.getText().toString().trim())) {
            Utils.showToastMessage(activity, "Phone Number is mandatory");
            return false;
        }

        if (!binding.etEnterPhoneNumber.getText().toString().trim().matches(Patterns.PHONE.pattern())) {
            Utils.showToastMessage(activity, "Phone Number should only be in digits");
            return false;
        }

        if (binding.etEnterPhoneNumber.getText().toString().trim().length() != 10) {
            Utils.showToastMessage(activity, "Phone Number should be 10 digits long");
            return false;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
