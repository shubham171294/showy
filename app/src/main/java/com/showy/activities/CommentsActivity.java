/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.showy.R;
import com.showy.adapter.CommentsAdapter;
import com.showy.databinding.ActivityCommentsBinding;
import com.showy.models.Comments;
import com.showy.models.CommentsData;
import com.showy.models.PostComment;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsActivity extends AppCompatActivity {

    private static final java.lang.String TAG = CommentsActivity.class.getSimpleName();
    private long feedId;
    private ActivityCommentsBinding binding;
    private ApiEndPointInterface apiEndPointInterface;
    private Call<Comments> commentsCall;
    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private ArrayList<CommentsData> mCommentsArrayList = new ArrayList<>();
    private CommentsAdapter mCommentsAdapter;
    private Call<PostComment> postCommentCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_comments);

        init();
        getDataFromIntent();
        fetchComments();
        setListeners();
    }

    private void setListeners() {
        binding.buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(binding.etAddComment.getText().toString().trim())) {
                    postComment(binding.etAddComment.getText().toString().trim());
                }
            }
        });
    }

    private void postComment(String comment) {
        postCommentCall = apiEndPointInterface.postCommentInFeed(mSharedPreferences.getLong(Constants.USER_ID, 0),
                comment, feedId);
        postCommentCall.enqueue(new Callback<PostComment>() {
            @Override
            public void onResponse(Call<PostComment> call, Response<PostComment> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {
                        PostComment.Data postCommentData = response.body().getData();
                        CommentsData addComment = new CommentsData();
                        addComment.setCreationTime(postCommentData.getCreationTime());
                        addComment.setUserPic(mSharedPreferences.getString(Constants.PROFILE_PIC, ""));
                        addComment.setUserId(postCommentData.getUserId());
                        addComment.setUserName(mSharedPreferences.getString(Constants.NAME, ""));
                        addComment.setComment(postCommentData.getComment());
                        addComment.setFeedId(postCommentData.getFeedId());
                        addComment.setId(postCommentData.getId());
                        mCommentsArrayList.add(addComment);
                        mCommentsAdapter.notifyItemInserted(mCommentsArrayList.size() - 1);
                        binding.etAddComment.getText().clear();
                        binding.tvNoData.setVisibility(View.GONE);
                        binding.rv.scrollToPosition(mCommentsArrayList.size() - 1);
                    } else {
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getReason());
                        else if (response.body().getMessage() != null && !response.body().getMessage().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getMessage());
                        else
                            Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));
                    }
                }
            }

            @Override
            public void onFailure(Call<PostComment> call, Throwable t) {
                Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));
                Utils.showLog(TAG, t.getMessage());
            }
        });
    }

    private void fetchComments() {
        commentsCall = apiEndPointInterface.getAllCommentsInFeed(feedId, mSharedPreferences.getLong(Constants.USER_ID, 0));
        commentsCall.enqueue(new Callback<Comments>() {
            @Override
            public void onResponse(Call<Comments> call, Response<Comments> response) {
                binding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {

                        ArrayList<CommentsData> commentsData = (ArrayList<CommentsData>) response.body().getData();
                        if (commentsData.size() != 0) {
                            binding.tvNoData.setVisibility(View.GONE);
                            mCommentsArrayList.addAll(commentsData);
                            mCommentsAdapter.notifyDataSetChanged();
                        } else
                            binding.tvNoData.setVisibility(View.VISIBLE);

                    } else {
                        binding.tvNoData.setVisibility(View.VISIBLE);
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getReason());
                        else if (response.body().getMessage() != null && !response.body().getMessage().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(mContext, response.body().getMessage());
                        else
                            Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));
                    }
                }
            }

            @Override
            public void onFailure(Call<Comments> call, Throwable t) {
                binding.progressBar.setVisibility(View.GONE);
                binding.tvNoData.setVisibility(View.VISIBLE);
                Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));
                Utils.showLog(TAG, t.getMessage());
            }
        });
    }

    private void init() {
        mContext = getApplicationContext();

        getSupportActionBar().setTitle("Comments");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        apiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
        mSharedPreferences = Utils.getSharedPrefs(mContext);
        mCommentsAdapter = new CommentsAdapter(mCommentsArrayList);
        binding.rv.setAdapter(mCommentsAdapter);
        binding.rv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            feedId = bundle.getLong("feed_id", 0);
        }
    }

    @Override
    protected void onDestroy() {
        commentsCall.cancel();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
