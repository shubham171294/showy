package com.showy.activities;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.showy.R;
import com.showy.databinding.ActivityVerifyOtpBinding;
import com.showy.models.Otp;
import com.showy.models.User;
import com.showy.models.UserData;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOtpActivity extends AppCompatActivity{

    private Activity activity;
    private ActivityVerifyOtpBinding binding;
    private boolean isBroadcastRegistered;
    private final int REQUEST_CODE = 926;
    private ApiEndPointInterface apiEndPointInterface;
    private String userName, userPic, firstName, email, phone, address;
    private double lat, lon;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
//    private String mFromWhere;

    /*private BroadcastReceiver smsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getStringExtra("otp");
            if (!TextUtils.isEmpty(a)) {
                //verified
                binding.etEnterOtp.setText(a);
            }
        }
    };*/
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                binding.etEnterOtp.setText(message);
                //Do whatever you want with the code here
            }
        }
    };

    private java.lang.String TAG = VerifyOtpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verify_otp);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        init();
        clickListeners();
        fetchDataFromPreviousActivity();
        askReadMessagesPermission();
    }

    private void fetchDataFromPreviousActivity() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userPic = bundle.getString("pic", "");
            userName = bundle.getString("name", "");
            email = bundle.getString("email", "");
            phone = bundle.getString("phone", "");
            address = bundle.getString("address", "");
            lat = bundle.getDouble("lat", 0);
            lon = bundle.getDouble("lon", 0);
//            mFromWhere = bundle.getString(Constants.FROM_WHERE);
        }
    }


    private void init() {
        activity = VerifyOtpActivity.this;

        getSupportActionBar().setTitle("Verify Otp");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        apiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
        mSharedPreferences = Utils.getSharedPrefs(activity);
        mEditor = mSharedPreferences.edit();
    }

    private void clickListeners() {
        binding.tvResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyMobile();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    /**
     * asks permission from the user to read messages
     */
    private void askReadMessagesPermission() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
//            registerReceiver(smsReceiver, new IntentFilter("otp"));
            LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));

            isBroadcastRegistered = true;
            verifyMobile();
        } else {
            // request for permission
            boolean isDeniedPreviously = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS);
            if (isDeniedPreviously) {
                // show an explanation as to why you need this permission and again request for permission
                // if don't ask again box is checked, and we have again asked for permission it will directly call
                // onRequestPermissionResult with Permission_DENIED result
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, REQUEST_CODE); // shows the dialog
            } else {
                // request for permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, REQUEST_CODE); // shows the dialog

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                registerReceiver(smsReceiver, new IntentFilter("otp"));
                LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
                isBroadcastRegistered = true;
                verifyMobile();
            } else {
//                 show dialog so that user manually enters OTP
                verifyMobile();

            }
        }
    }

    private void verifyMobile() {
        Utils.showDialog(activity, getString(R.string.loading));
        Call<Otp> otpCall = apiEndPointInterface.getOtp(phone);

        otpCall.enqueue(new Callback<Otp>() {
            @Override
            public void onResponse(Call<Otp> call, final Response<Otp> response) {

                Utils.hideDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {

                        final String receivedOtp = response.body().getData();
//                        Utils.showToastMessage(activity, receivedOtp);
                        Utils.showLog(TAG, "otp: " + receivedOtp);

                        binding.buttonSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (binding.etEnterOtp.getText().toString().trim().equalsIgnoreCase(receivedOtp)) {
                                    //verified
                                    if (response.body().getExist().trim().equalsIgnoreCase("1")) {
                                        try {
                                            updateFcmTokenOfOldUser(response.body().getUser());
                                        } catch (Exception e) {
                                            Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
                                            Utils.showLog(TAG, "fcm token error: " + e.getMessage());
                                        }
                                    } else {
                                        createNotRegisteredDialog();
                                    }


                                } else {
                                    Utils.showToastMessage(activity, "OTP not correct");
                                }
                            }
                        });

                    } else {
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(activity, response.body().getReason());
                        else if (response.body().getMessage() != null && !response.body().getMessage().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(activity, response.body().getMessage());
                        else
                            Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
                    }
                }
            }

            @Override
            public void onFailure(Call<Otp> call, Throwable t) {
                Utils.hideDialog();
                Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
                Utils.showLog(TAG, t.getMessage());
            }
        });
    }

    /**
     * shows dialog when user has not registered but still try to login
     */
    private void createNotRegisteredDialog() {
        new AlertDialog.Builder(VerifyOtpActivity.this)
                .setMessage("You are not registered. Kindly register first")
                .setPositiveButton("Go to Registration", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        mEditor.putString(Constants.PHONE_NUMBER, phone).apply();
                        Intent i = new Intent(activity, SignUpOptionsActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void updateFcmTokenOfOldUser(UserData user) throws Exception {
        Utils.showDialog(VerifyOtpActivity.this, getString(R.string.loading));
        Call<User> updateFcmCall = apiEndPointInterface.updateFcmToken(user.getId(), FirebaseInstanceId.getInstance().getToken());
        updateFcmCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Utils.hideDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {
                        // old user has login, therefore no need to create a new user
                        // only save his old data
                        saveUserDataInPrefs(response.body().getData());
                    } else {
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(activity, response.body().getReason());
                        else if (response.body().getMessage() != null && !response.body().getMessage().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(activity, response.body().getMessage());
                        else
                            Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Utils.hideDialog();
                t.printStackTrace();
            }
        });
    }

    /**
     * save user data in shared preferences
     * this data can be of old user or a new user
     *
     * @param userData user data
     */
    private void saveUserDataInPrefs(UserData userData) {

        mEditor.putLong(Constants.USER_ID, userData.getId());
        mEditor.putString(Constants.PROFILE_PIC, userData.getUserPic());
        mEditor.putString(Constants.NAME, userData.getUserName());
        mEditor.putString(Constants.EMAIL, userData.getEmail());
        mEditor.putString(Constants.PHONE_NUMBER, userData.getPhone());
        mEditor.putString(Constants.ADDRESS, userData.getAddress());
        mEditor.putString(Constants.LAT, String.valueOf(userData.getLat()));
        mEditor.putString(Constants.LON, String.valueOf(userData.getLon()));
        mEditor.putBoolean(Constants.IS_LOGIN, true);
        mEditor.apply();
        Intent i = new Intent(activity, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }


    @Override
    protected void onStop() {
        if (isBroadcastRegistered) {
//            unregisterReceiver(smsReceiver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
            isBroadcastRegistered = false;
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (isBroadcastRegistered) {
//            unregisterReceiver(smsReceiver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
            isBroadcastRegistered = false;
        }
        super.onDestroy();
    }

}
