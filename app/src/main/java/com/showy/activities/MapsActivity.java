///*
// *
// *  * Copyright (C) 2017 The Android Open Source Project
// *  *
// *  * Licensed under the Apache License, Version 2.0 (the "License");
// *  * you may not use this file except in compliance with the License.
// *  * You may obtain a copy of the License at
// *  *
// *  *      http://www.apache.org/licenses/LICENSE-2.0
// *  *
// *  * Unless required by applicable law or agreed to in writing, software
// *  * distributed under the License is distributed on an "AS IS" BASIS,
// *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// *  * See the License for the specific language governing permissions and
// *  * limitations under the License.
// *
// */
//
//package com.showy.activities;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.location.Address;
//import android.location.Geocoder;
//import android.location.Location;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AutoCompleteTextView;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.PendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.PlaceBuffer;
//import com.google.android.gms.location.places.Places;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.CameraPosition;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//
//import java.util.List;
//import java.util.Locale;
//
//import app.showcraze.adapters.AutocompleteAdapter;
//import app.showcraze.utils.LocationUtils;
//
//public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener,
//        GoogleApiClient.ConnectionCallbacks {
//
//    public static final int PERMISSION_REQUEST_CODE = 0;
//    private static final LatLngBounds BOUNDS_CUSTOM = new LatLngBounds(
//            new LatLng(9.0, 68.0), new LatLng(36.0, 98.0));
//    private static final String TAG = MapsActivity.class.getSimpleName();
//    protected DrawerLayout drawerLayout;
//    protected GoogleApiClient mGoogleApiClient;
//    List<Address> address;
//    double longitude, latitude;
//    TextView addressTV;
//    ImageView setLatLongIV;
//    LatLng center;
//    String addresss, countryname, city, pincode, state;
//    SupportMapFragment mapFragment;
//    private ListView navigationLV;
//    //    private NavigationFragment mNavigationFragment;
//    private Toolbar toolbar;
//    private TextView toolbarTV;
//    private RecyclerView recyclerView;
//    private LinearLayoutManager linearLayoutManager;
//    private GoogleMap mMap;
//    private Intent returnIntent;
//    private AutoCompleteTextView autocompleteView;
//    // private AutoCompleteTextView mAutocompleteView;
//    private AutocompleteAdapter mAdapter;
//    private Place chosenPlace;
//    private Location lastLoc;
//
//    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
//        @Override
//        public void onResult(PlaceBuffer places) {
//            if (!places.getStatus().isSuccess()) {
//                Log.d(TAG, places.getStatus().toString());
//                return;
//            }
//            final Place place = places.get(0);
//
//            chosenPlace = place;
//
//            mMap.clear();
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                    new LatLng(place.getLatLng().latitude,
//                            place.getLatLng().longitude), 16));
//
//        }
//    };
//    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position,
//                                long id) {
//
//            final AutocompleteAdapter.PlaceAutocomplete item = mAdapter
//                    .getItem(position);
//            final String placeId = String.valueOf(item.placeId);
//
//            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
//                    .getPlaceById(mGoogleApiClient, placeId);
//
//
//            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
////            Toast.makeText(getApplicationContext(),
////                    "Clicked: " + item.description, Toast.LENGTH_SHORT).show();
//            Log.i("Places", "Called getPlaceById to get Place details for "
//                    + item.placeId);
//         /*   InputMethodManager imm = (InputMethodManager) getSystemService(
//                    Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), MODE_PRIVATE);*/
//        }
//    };
//
//    protected synchronized void rebuildGoogleApiClient() {
//
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, 0 /* clientId */, this)
//                .addConnectionCallbacks(this).addApi(Places.GEO_DATA_API)
//                .addApi(LocationServices.API).build();
//
//
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(app.showcraze.R.layout.activity_maps);
//        init();
//        addListener();
//        resetMyPositionButton();
//
//        if (Build.VERSION.SDK_INT < 23) {
//            mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                    .findFragmentById(app.showcraze.R.id.map);
//            mapFragment.getMapAsync(this);
//        } else {
//            if (checkGPSPermission(this)) {
//                mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                        .findFragmentById(app.showcraze.R.id.map);
//                mapFragment.getMapAsync(this);
//            }
//        }
//    }
//
//    private void addListener() {
//        setLatLongIV.setOnClickListener(this);
//    }
//
//    public boolean checkGPSPermission(Context context) {
//
//        if (Build.VERSION.SDK_INT < 23) {
////            Toast.makeText(context, "lower version", Toast.LENGTH_SHORT).show();
//            return true;
//        } else {
////            Toast.makeText(context, "marshmallow or higher", Toast.LENGTH_SHORT).show();
//            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
////                Toast.makeText(context, "permission granted", Toast.LENGTH_SHORT).show();
//                return true;
//            } else {
////                Toast.makeText(context, "no permission granted", Toast.LENGTH_SHORT).show();
//                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
//                    Toast.makeText(context, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
//                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MapsActivity.PERMISSION_REQUEST_CODE);
//                } else {
//                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MapsActivity.PERMISSION_REQUEST_CODE);
//                }
//            }
//        }
//        return false;
////        return true;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                            .findFragmentById(app.showcraze.R.id.map);
//                    mapFragment.getMapAsync(this);
////                    Toast.makeText(MainActivity.this, "permission success", Toast.LENGTH_SHORT).show();
//                } else {
////                    Toast.makeText(MainActivity.this, "permission fail", Toast.LENGTH_SHORT).show();
//                }
//                break;
//        }
//    }
//
//    private void init() {
//        addressTV = (TextView) findViewById(app.showcraze.R.id.addressTV);
//
//        toolbar = (Toolbar) findViewById(app.showcraze.R.id.toolbar);
////        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//        toolbarTV = (TextView) findViewById(app.showcraze.R.id.toolbarTV);
//        toolbarTV.setText("Location");
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//
//        if (mGoogleApiClient == null) {
//            rebuildGoogleApiClient();
//        }
//
//        setLatLongIV = (ImageView) findViewById(app.showcraze.R.id.setLatLongIV);
//        returnIntent = new Intent();
//
//        AutoCompleteTextView autocompleteView = (AutoCompleteTextView) findViewById(app.showcraze.R.id.search_bar);
//
//        mAdapter = new AutocompleteAdapter(this, BOUNDS_CUSTOM, null);
//        autocompleteView.setOnItemClickListener(mAutocompleteClickListener);
//        autocompleteView.setAdapter(mAdapter);
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        LocationUtils utils = new LocationUtils();
//
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                new LatLng(utils.getlocation(this).latitude, utils.getlocation(this).longitude), 16));
//        mMap.setMyLocationEnabled(true);
//
//        latitude = utils.getlocation(this).latitude;
//        longitude = utils.getlocation(this).longitude;
//
//        // Polylines are useful for marking paths and routes on the map.
//        center = mMap.getCameraPosition().target;
//
////                map.addPolyline(new PolylineOptions().geodesic(true)
////                        .add(new LatLng(-33.866, 151.195))  // Sydney
////                        .add(new LatLng(-18.142, 178.431))  // Fiji
////                        .add(new LatLng(21.291, -157.821))  // Hawaii
////                        .add(new LatLng(37.423, -122.091))  // Mountain View
////                );
////                BitmapDescriptor image=BitmapDescriptorFactory.fromResource(R.drawable.googleicon);
////                GroundOverlayOptions groundoverlay=new GroundOverlayOptions()
////                        .image(image)
////                        .position(center,500f)
////                        .transparency(0.05f);
////                map.addGroundOverlay(groundoverlay);
//
//
//        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//            @Override
//            public void onCameraChange(CameraPosition cameraPosition) {
//
//                longitude = cameraPosition.target.longitude;
//                latitude = cameraPosition.target.latitude;
//                Log.e("longitude", longitude + "");
//                Log.e("latitude", latitude + "");
//                try {
//                    address = new Geocoder(MapsActivity.this, Locale.getDefault()).getFromLocation(latitude, longitude, 1);
//                    Log.e("longitude address", longitude + "");
//                    Log.e("latitude address", latitude + "");
//                    Log.e("Address ", address + "");
//
//                    addresss = address.get(0).getAddressLine(0);
//                    city = address.get(0).getLocality();
//                    state = address.get(0).getAdminArea();
//                    countryname = address.get(0).getCountryName();
//                    pincode = address.get(0).getPostalCode();
//                    latitude = address.get(0).getLatitude();
//                    longitude = address.get(0).getLongitude();
//                    addressTV.setText(addresss + ", " + city + ", " + state + ", " + countryname);
//
////                    mMap.addMarker(new MarkerOptions()
//////                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.googleicon))
////                            .title("Address")
////                            .snippet(addresss + " " + city + " " + state + " " + countryname + " " + pincode)
////                            .position(center));
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//
//
//    }
//
//    private void sendResultToParentActivity() {
//        returnIntent.putExtra("latitude", "" + latitude);
//        returnIntent.putExtra("longitude", "" + longitude);
//        returnIntent.putExtra("address", addresss);
//        returnIntent.putExtra("city", city);
//        returnIntent.putExtra("full_address", addressTV.getText().toString().trim());
//
//        setResult(Activity.RESULT_OK, returnIntent);
//        finish();
//
//    }
//
//    @Override
//    public void onClick(View v) {
//
//        sendResultToParentActivity();
//
//    }
//
//    private void resetMyPositionButton() {
//        Fragment fragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(app.showcraze.R.id.map));
//        ViewGroup v1 = (ViewGroup) fragment.getView();
//        ViewGroup v2 = (ViewGroup) v1.getChildAt(0);
//        ViewGroup v3 = (ViewGroup) v2.getChildAt(2);
//        View position = (View) v3.getChildAt(0);
//        int positionWidth = position.getLayoutParams().width;
//        int positionHeight = position.getLayoutParams().height;
//
//        //lay out position button
//        RelativeLayout.LayoutParams positionParams = new RelativeLayout.LayoutParams(positionWidth, positionHeight);
//        int margin = positionWidth / 5;
//        positionParams.setMargins(margin, 0, 0, margin);
//        positionParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
//        positionParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
//        position.setLayoutParams(positionParams);
//    }
//
//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//        mAdapter.setGoogleApiClient(mGoogleApiClient);
//       /* lastLoc = LocationServices.FusedLocationApi
//                .getLastLocation(mGoogleApiClient);
//
//        if (mMapFragment.getView().getViewTreeObserver().isAlive()) {
//            com.google.android.gms.maps.GoogleMap gmap = mMapFragment.getMap();
//            gmap.setMyLocationEnabled(true);
//            if (lastLoc != null) {
//                gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
//                        lastLoc.getLatitude(), lastLoc.getLongitude()), Constant.MAP_ZOOM_LABEL));
//            }
//            onCurrentLocFound();
//        } else {
//            mMapFragment.getMap().setOnMapLoadedCallback(
//                    new com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback() {
//                        @Override
//                        public void onMapLoaded() {
//                            com.google.android.gms.maps.GoogleMap gmap = mMapFragment.getMap();
//                            gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                                    new LatLng(lastLoc.getLatitude(), lastLoc
//                                            .getLongitude()), Constant.MAP_ZOOM_LABEL));
//                            onCurrentLocFound();
//                        }
//                    });
//        }*/
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//        mAdapter.setGoogleApiClient(null);
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        Toast.makeText(
//                this,
//                "Could not connect to Google API Client: Error "
//                        + connectionResult.getErrorCode(), Toast.LENGTH_SHORT)
//                .show();
//
//        mAdapter.setGoogleApiClient(null);
//    }
//}