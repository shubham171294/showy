package com.showy.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.showy.R;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

public class SplashScreenActivity extends AppCompatActivity {

    private Activity activity;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getSupportActionBar().hide();

        activity = SplashScreenActivity.this;

        mSharedPreferences = Utils.getSharedPrefs(getApplicationContext());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                mSharedPreferences.edit().putInt(Constants.USER_SELECTED_COLOR, 0).apply();
                if (mSharedPreferences.getBoolean(Constants.IS_LOGIN, false)) {
                    Intent i = new Intent(activity, HomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else {
                    Intent i = new Intent(activity, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
        }, 3 * 1000);
    }
}
