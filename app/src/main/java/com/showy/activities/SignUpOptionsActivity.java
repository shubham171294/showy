/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package com.showy.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.showy.R;
import com.showy.databinding.ActivitySignUpOptionsBinding;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/***
 * class to show options for signup. There is facebook signup and custom signup option.
 */
public class SignUpOptionsActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private final String TAG = SignUpOptionsActivity.class.getSimpleName();
    private ActivitySignUpOptionsBinding binding;
    private int dotsCount;
    private ImageView[] dots;
    private ViewPagerAdapter mAdapter;
    private Context mContext;
    private CallbackManager callbackManager;

    private int[] mImageResources = {
            R.drawable.img1,
            R.drawable.img2,
            R.drawable.img3,

    };
    private String personName;
    private String profilePic;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // To make mContext full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_options);
        init();
        clickListeners();
        handleFbLoginResult();
    }

    private void init() {
        mContext = getApplicationContext();
        getSupportActionBar().hide();

        mSharedPreferences = Utils.getSharedPrefs(mContext);
        mEditor = mSharedPreferences.edit();
        mAdapter = new ViewPagerAdapter(mContext, mImageResources);
        binding.pagerIntroduction.setAdapter(mAdapter);
        binding.pagerIntroduction.setCurrentItem(0);
        binding.pagerIntroduction.addOnPageChangeListener(this);
        setUiPageViewController();
    }

    private void clickListeners() {
        binding.fbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(mContext, VerifyOtpActivity.class);
//                startActivity(i);
                LoginManager.getInstance().logInWithReadPermissions(
                        SignUpOptionsActivity.this,
                        Arrays.asList("email", "public_profile")
                );
            }
        });

        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, SignUpActivity.class);
                startActivity(i);
            }
        });

        binding.tvAlreadyLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, LoginActivity.class));
            }
        });
    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.non_selected_item_dot));


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            binding.viewPagerCountDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.selected_item_dot));

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.non_selected_item_dot));
        }

        dots[position].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.selected_item_dot));

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * handles the result from the login of facebook
     */
    private void handleFbLoginResult() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(
                callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // Handle success
                        Utils.showLog(TAG, loginResult.toString());
                        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {


                                Log.e("graphresponse", graphResponse.toString());
                                Log.e("jsonresponse", jsonObject.toString());

                                sendUserDetailsToServer(jsonObject, graphResponse);

                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "name,email,picture");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Utils.showToastMessage(mContext, "Facebook login cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Utils.showToastMessage(mContext, getString(R.string.some_error_occurred));
                        Utils.showLog(TAG, "fb error : " + exception.getMessage());
                    }
                }
        );
    }

    private void sendUserDetailsToServer(final JSONObject jsonObject, GraphResponse graphResponse) {

        try {
            personName = jsonObject.getString("name");

            String rawResponse = graphResponse.getRawResponse();
            JSONObject rawJsonResponse = new JSONObject(rawResponse);
            if (rawJsonResponse.has("picture")) {
                profilePic = rawJsonResponse.getJSONObject("picture").getJSONObject("data").getString("url");
                mEditor.putString(Constants.PROFILE_PIC, profilePic.trim());
            } else {
                mEditor.putString(Constants.PROFILE_PIC, "");
            }

            String email = "";
            if (jsonObject.getString("email") != null) {
                email = jsonObject.getString("email");
            }

            Intent i = new Intent(mContext, SignUpActivity.class);
            i.putExtra("name", personName);
            i.putExtra("pic", profilePic);
            i.putExtra("email", email);
            startActivity(i);

        } catch (JSONException e) {
            e.printStackTrace();
            Utils.hideDialog();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    class ViewPagerAdapter extends PagerAdapter {

        private Context mContext;
        private int[] mResources;

        public ViewPagerAdapter(Context mContext, int[] mResources) {
            this.mContext = mContext;
            this.mResources = mResources;
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);
            imageView.setImageResource(mResources[position]);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}
