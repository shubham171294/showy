/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.activities;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.bumptech.glide.Glide;
import com.showy.R;
import com.showy.databinding.ActivityFeedDetailBinding;
import com.showy.utils.Utils;

public class FeedDetailActivity extends AppCompatActivity {

    private static final java.lang.String TAG = FeedDetailActivity.class.getSimpleName();
    private long feedId;
    private String productId;
    private ActivityFeedDetailBinding binding;
    private String productPic;
    private Context mContext;
    private String productUrl, allDesignsUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feed_detail);
        init();
        getDataFromIntent();
        binding.tvProductId.setText(String.valueOf("#" + productId));
        if (!TextUtils.isEmpty(productPic)) {
            Glide.with(mContext).load(productPic).placeholder(R.drawable.placeholder).into(binding.ivFeedPic);
        }
        setListeners();
    }

    private void setListeners() {
        binding.productLink1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(productUrl)){
                    Utils.openBrowser(FeedDetailActivity.this, productUrl);
                }
            }
        });

        binding.tvAllDesigns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openBrowser(FeedDetailActivity.this, allDesignsUrl);
            }
        });
    }

    private void init() {
        mContext = getApplicationContext();
        getSupportActionBar().setTitle("Design Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        allDesignsUrl = "https://paytm.com/shop/search?q=showy%20designer%20nails&amp;from=organic&amp;child_site_id=1&amp;site_id=1";
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            productId = bundle.getString("product_id", "");
            productPic = bundle.getString("pic", "");
            feedId = bundle.getLong("feed_id", 0);
            productUrl = "https://paytm.com/shop/search?q=showy%20"+ productId + "&amp;from=organic&amp;child_site_id=1&amp;site_id=1";
            Utils.showLog(TAG, productUrl);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
