/*
 *
 *  * Copyright (C) 2017 The Android Open Source Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.showy.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;

import com.showy.R;
import com.showy.adapter.ColorSelectionAdapter;
import com.showy.databinding.ActivityMatchYourNailBinding;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.util.ArrayList;

public class MatchYourNailActivity extends AppCompatActivity {

    private ActivityMatchYourNailBinding binding;
    private ArrayList<Integer> colorsList;
    private Dialog dialog;
    private Context activity;
    private int color;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_match_your_nail);
        init();
        setListeners();
    }


    private void init() {

        activity = getApplicationContext();
        colorsList = Utils.getColorList();
        binding.ivAlpha.setAlpha(0f);
        binding.seekBar.setMax(80);
        mSharedPreferences = Utils.getSharedPrefs(activity);
    }

    private void setListeners() {


        binding.ivColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createColorSelectionDialog();
            }
        });

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {

                if (fromUser)
                    binding.ivAlpha.setAlpha((float) i / 100);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.buttonApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSharedPreferences.edit().putInt(Constants.USER_SELECTED_COLOR, color).apply();
                Intent i = new Intent(activity,HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });
    }

    private void createColorSelectionDialog() {
        dialog = new Dialog(MatchYourNailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_color_chooser);
        dialog.show();

        Button allDesigns = (Button) dialog.findViewById(R.id.button_all_designs);
        allDesigns.setVisibility(View.GONE);
        RecyclerView rv = (RecyclerView) dialog.findViewById(R.id.rv);
        rv.setLayoutManager(new GridLayoutManager(activity, 4));
        rv.setAdapter(new ColorSelectionAdapter(colorsList, new ColorSelectionAdapter.OnColorSelected() {
            @Override
            public void onColorSelectedListener(int position) {
                if (dialog != null)
                    dialog.cancel();

                binding.ivNailColor.setImageDrawable(null);
                binding.ivColor.setImageDrawable(ContextCompat.getDrawable(activity, colorsList.get(position)));
                switch (position) {
                    case 0:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.red));
                        color = Constants.Red;
                        break;
                    case 1:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.orenge));
                        color = Constants.Orange;
                        break;
                    case 2:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.yellow));
                        color = Constants.Yellow;
                        break;
                    case 3:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.lightsalmon));
                        color = Constants.Lightsalmon;
                        break;
                    case 4:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.light_coral));
                        color = Constants.Lightcoral;
                        break;
                    case 5:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.brown));
                        color = Constants.Brown;
                        break;
                    case 6:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.maroon));
                        color = Constants.Maroon;
                        break;
                    case 7:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.black));
                        color = Constants.Black;
                        break;
                    case 8:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.dark_grey));
                        color = Constants.Dark_Grey;
                        break;
                    case 9:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ivory));
                        color = Constants.Ivory;
                        break;
                    case 10:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.forest_green));
                        color = Constants.Forest_Green;
                        break;
                    case 11:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pale_green));
                        color = Constants.Pale_Green;
                        break;
                    case 12:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.medium_aqua_marine));
                        color = Constants.Medium_Aqua_Marine;
                        break;
                    case 13:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.medium_turquoise));
                        color = Constants.Medium_Turquoise;
                        break;
                    case 14:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.medium_vilot_red));
                        color = Constants.Medium_Vilot_Red;
                        break;
                    case 15:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.magenta));
                        color = Constants.Magenta;
                        break;
                    case 16:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.violet));
                        color = Constants.Violet;
                        break;
                    case 17:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hot_pink));
                        color = Constants.Hot_Pink;
                        break;
                    case 18:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pink));
                        color = Constants.Pink;
                        break;
                    case 19:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.rosy_brown));
                        color = Constants.Rosy_Brown;
                        break;
                    case 20:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.sapphire));
                        color = Constants.Sapphire;
                        break;
                    case 21:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.blueviolet));
                        color = Constants.Blueviolet;
                        break;
                    case 22:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.royal_blue));
                        color = Constants.Royal_Blue;
                        break;
                    case 23:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.sky_blue));
                        color = Constants.Sky_Blue;
                        break;
                    case 24:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.lavender));
                        color = Constants.Lavender;
                        break;
                    default:
                        binding.ivNailColor.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.red));
                        color = Constants.Red;

                }
            }
        }));

    }
}
