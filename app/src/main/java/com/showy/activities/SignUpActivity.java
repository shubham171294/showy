package com.showy.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.iid.FirebaseInstanceId;
import com.showy.R;
import com.showy.databinding.ActivitySignUpBinding;
import com.showy.models.PicUpload;
import com.showy.models.User;
import com.showy.models.UserData;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    private final String TAG = SignUpActivity.class.getSimpleName();
    private ActivitySignUpBinding binding;
    private Context activity;
    private double latitude;
    private double longitude;
    private String address;
    private GoogleApiAvailability googleApiInstance;
    private Dialog dialog;
    private String captured_image;
    private String imagePath;
    private ApiEndPointInterface mApiEndPointInterface;
    private String uploadFileUrl;
    private boolean isPlacePickerOpened;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private String phone;
    private String fbImagePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        init();
        clickListeners();
        fetchDataFromIntent();
    }

    /**
     * get data when user has login through facebook
     */
    private void fetchDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            binding.userName.setText(bundle.getString("name"));
            binding.userEmail.setText(bundle.getString("email"));
            fbImagePath = bundle.getString("pic");
            Glide.with(activity).load(fbImagePath).asBitmap().centerCrop().into(new BitmapImageViewTarget(binding.userImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    super.setResource(resource);
                    saveProfilePicBitmapInInternalStorage(resource);
                }
            });
            uploadFileUrl = fbImagePath; // will become the url of user uploaded pic
        }
    }


    private void init() {
        activity = getApplicationContext();
        getSupportActionBar().hide();

        googleApiInstance = GoogleApiAvailability.getInstance();
        mApiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
        mSharedPreferences = Utils.getSharedPrefs(activity);
        mEditor = mSharedPreferences.edit();

        phone = mSharedPreferences.getString(Constants.PHONE_NUMBER, "");
        binding.userPhone.setText(phone);
        binding.userPhone.setEnabled(false);
    }

    private void clickListeners() {
        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (areAllFieldsValid()) {
                    uploadPic();
                }
            }
        });

        binding.etUserLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivityForResult(new Intent(activity, MapsActivity.class), Constants.USER_LOC_REQUEST_CODE);
                if (!isPlacePickerOpened) {
                    isPlacePickerOpened = true;
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    try {
                        startActivityForResult(builder.build(SignUpActivity.this), Constants.USER_LOC_REQUEST_CODE);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                        isPlacePickerOpened = false;
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                        isPlacePickerOpened = false;
                    }
                }


            }
        });

        binding.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askPermission();
            }
        });
    }

    private boolean areAllFieldsValid() {

        if (TextUtils.isEmpty(binding.userName.getText().toString().trim())) {
            Utils.showToastMessage(activity, "Username is mandatory");
            return false;
        }
        if (TextUtils.isEmpty(binding.userEmail.getText().toString().trim())) {
            Utils.showToastMessage(activity, "Email is mandatory");
            return false;
        }

        if (!binding.userEmail.getText().toString().trim().matches(Patterns.EMAIL_ADDRESS.pattern())) {
            Utils.showToastMessage(activity, "Email should be in proper format");
            return false;
        }

        if (TextUtils.isEmpty(binding.userPhone.getText().toString().trim())) {
            Utils.showToastMessage(activity, "Phone Number is mandatory");
            return false;
        }

        if (!binding.userPhone.getText().toString().trim().matches(Patterns.PHONE.pattern())) {
            Utils.showToastMessage(activity, "Phone Number should only be a number");
            return false;
        }

        if (binding.userPhone.getText().toString().trim().length() != 10) {
            Utils.showToastMessage(activity, "Phone Number should be 10 digits long");
            return false;
        }

        if (latitude == 0 && longitude == 0) {
            Utils.showToastMessage(activity, "Location is mandatory");
            return false;
        }

        /*if (TextUtils.isEmpty(imagePath)) {
            Utils.showToastMessage(activity, "Profile picture is mandatory");
            return false;
        }*/
        return true;
    }


    private void askPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            // perform your work
            initiateEditProfilePicImagePopupWindow();
        } else {
            // request for permission
            boolean isDeniedPreviously = ActivityCompat.shouldShowRequestPermissionRationale(SignUpActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (isDeniedPreviously) {
                // show an explanation as to why you need EditUserProfileFragment permission and again request for permission
                // if don't ask again box is checked, and we have again asked for permission it will directly call
                // onRequestPermissionResult with Permission_DENIED result
                new AlertDialog.Builder(activity)
                        .setTitle(Constants.STORAGE_PERMISSION)
                        .setMessage(Constants.STORAGE_PERMISSION_RATIONALE)
                        .setCancelable(true)
                        .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Utils.startInstalledAppDetailsActivity(activity);
                            }
                        })
                        .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                Utils.showToastMessage(activity, getString(R.string.permission_denied));
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            } else {
                // request for permission
                ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.GET_PROFILE_PIC_STORAGE_PERMISSION); // shows the dialog

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case Constants.GET_PROFILE_PIC_STORAGE_PERMISSION:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initiateEditProfilePicImagePopupWindow();
                } else {
                    new AlertDialog.Builder(activity)
                            .setTitle(Constants.STORAGE_PERMISSION)
                            .setMessage(Constants.STORAGE_PERMISSION_RATIONALE)
                            .setCancelable(false)
                            .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Utils.startInstalledAppDetailsActivity(activity);
                                }
                            })
                            .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    Utils.showToastMessage(activity, getString(R.string.permission_denied));
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
                break;
            // other 'case' lines to check for other
        }
    }

    private void initiateEditProfilePicImagePopupWindow() {

        dialog = new Dialog(SignUpActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_media_chooser);
        dialog.show();
        TextView cameraSelect = (TextView) dialog.findViewById(R.id.tv_choose_camera);
        TextView gallerySelect = (TextView) dialog.findViewById(R.id.tv_choose_gallery);

        cameraSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                captured_image = System.currentTimeMillis() + ".jpg";
                File file = new File(Environment.getExternalStorageDirectory(), captured_image);
                captured_image = file.getAbsolutePath();
                Uri outputFileUri = Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                startActivityForResult(cameraIntent, Constants.CAMERA_SELECT_REQUEST_CODE);

            }
        });

        gallerySelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, Constants.GALLERY_SELECT_REQUEST_CODE);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.USER_LOC_REQUEST_CODE && resultCode == RESULT_OK) {

//            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(activity, GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity), requestCode, new DialogInterface.OnCancelListener() {
//                @Override
//                public void onCancel(DialogInterface dialogInterface) {
//
//                }
//            });
//            dialog.show();

            isPlacePickerOpened = false;
            googleApiInstance.showErrorDialogFragment(SignUpActivity.this, googleApiInstance.isGooglePlayServicesAvailable(activity), requestCode);//            latitude = String.valueOf(data.getDoubleExtra("latitude", 0));


//            latitude = data.getStringExtra("latitude");
////            longitude = String.valueOf(data.getDoubleExtra("longitude", 0));
//            longitude = data.getStringExtra("longitude");
//            String temp = data.getStringExtra("address");
//            Utils.showLog(TAG, "temp address: " + temp);
////            address = data.getStringExtra("full_address");
//            Utils.showLog(TAG, "full address: " + address);
//
//            String city = data.getStringExtra("city");
//            binding.tvUserLocation.setText(city);
//            address = city;
//            Utils.showLog("user location: " + latitude + "  " + longitude + "  " + address + "  " + city);
            Place place = PlacePicker.getPlace(data, this);
            Utils.showLog(TAG, "latitude: " + place.getLatLng().latitude);
            latitude = place.getLatLng().latitude;
            Utils.showLog(TAG, "longitude: " + place.getLatLng().longitude);
            longitude = place.getLatLng().longitude;

            String toastMsg = String.format("Place: %s", place.getAddress());
            binding.etUserLocation.setText(place.getAddress());
            address = place.getAddress().toString();
//            Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
        }

        // select profile pic from camera app
        if (requestCode == Constants.CAMERA_SELECT_REQUEST_CODE && resultCode == RESULT_OK) {
            if (dialog != null) {
                dialog.cancel();

                String imagePath = Utils.compressImage(captured_image);
                setPicture(imagePath, binding.userImage);
                /*try {

                    imageUri = Uri.parse(imagePath);
                } catch (Exception e) {
                    setPicture(captured_image, binding.ivUploadedPic);
                    imageUri = Uri.parse(captured_image);
                }*/
            }
        } else {
            if (dialog != null)
                dialog.cancel();
        }

        if (requestCode == Constants.GALLERY_SELECT_REQUEST_CODE && resultCode == RESULT_OK) {
            if (dialog != null) {
                dialog.cancel();
                if (data != null) {

                    Uri selectedUri = data.getData();
                    String pathforcalculatesize = Utils.getRealPathFromURI(selectedUri, activity);
                    String compressedImagePath = Utils.compressImage(pathforcalculatesize);
                    setPicture(compressedImagePath, binding.userImage);

                    /*profilePicFile = new File(pathforcalculatesize);


                    long fileSizeInBytes = profilePicFile.length();
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    if (fileSizeInKB < 1024) {
                        setPicture(pathforcalculatesize, binding.ivUploadedPic);
                        imageUri = Uri.fromFile(profilePicFile);
                    } else {
                        try {
                            String imagePath = Utils.compressImage(profilePicFile.getAbsolutePath());
                            setPicture(imagePath, binding.ivUploadedPic);
                            imageUri = Uri.parse(imagePath);
                        } catch (Exception e) {
                            setPicture(profilePicFile.getAbsolutePath(), binding.ivUploadedPic);
                            imageUri = Uri.fromFile(profilePicFile);
                        }
                    }
                    profilePicFile = null;*/
                } else {
                    // show error or do nothing
                }
            }
        }
    }

    private void setPicture(String imagePath, ImageView imageView) {
        Glide.with(activity).load(imagePath).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                super.setResource(resource);
                saveProfilePicBitmapInInternalStorage(resource);
            }
        });
        this.imagePath = imagePath;
    }

    private void uploadPic() {

        if (!TextUtils.isEmpty(imagePath)) {
            //upload pic from galley or camera
            Utils.showDialog(SignUpActivity.this, getString(R.string.loading));
            File file = new File(imagePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("file", file.getName(), requestFile);

            Call<PicUpload> picUploadCall = mApiEndPointInterface.postImage(body);
            picUploadCall.enqueue(new Callback<PicUpload>() {
                @Override
                public void onResponse(Call<PicUpload> call, Response<PicUpload> response) {

                    Utils.hideDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getStatus().trim().equalsIgnoreCase("success")) {
                            uploadFileUrl = response.body().getData();
                            Utils.showLog(TAG, uploadFileUrl);

                            try {
                                createUser();
                            } catch (Exception e) {
                                Utils.showLog(TAG, "fcm token not obtained");
                                e.printStackTrace();
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<PicUpload> call, Throwable t) {
                    Utils.hideDialog();
                    t.printStackTrace();
                }
            });
        } else if (!TextUtils.isEmpty(fbImagePath)) {

            // fb profile pic url obtained so continue without uploading
            try {
                createUser();
            } catch (Exception e) {
                Utils.showLog(TAG, "fcm token not obtained");
                e.printStackTrace();
            }
        } else {
            // no profile pic added, so continue without uploading
            try {
                uploadFileUrl = "";
                createUser();
            } catch (Exception e) {
                Utils.showLog(TAG, "fcm token not obtained");
                e.printStackTrace();
            }
        }
    }

    private void createUser() throws Exception {

        Utils.showDialog(SignUpActivity.this, getString(R.string.loading));

        Call<User> createUserCall = mApiEndPointInterface.createUser(
                binding.userName.getText().toString().trim(),
                binding.userEmail.getText().toString().trim(),
                binding.userPhone.getText().toString().trim(),
                uploadFileUrl,
                address,
                latitude,
                longitude,
                FirebaseInstanceId.getInstance().getToken()
        );
        createUserCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Utils.hideDialog();
                if (response.isSuccessful()) {

                    User user = response.body();
                    if (user.getStatus().trim().equalsIgnoreCase("success")) {
                        saveUserDataInPrefs(user.getData());
                    } else {
                        if (!TextUtils.isEmpty(response.body().getReason()))
                            Utils.showToastMessage(activity, response.body().getReason());
                        else if (!TextUtils.isEmpty(response.body().getMessage()))
                            Utils.showToastMessage(activity, response.body().getMessage());
                        else
                            Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Utils.hideDialog();
                Utils.showLog(TAG, t.getMessage());
                Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
            }
        });

    }

    /**
     * saves user profile pic as a bitmap in internal storage
     * this storage doesn't require any permissions as it is private to application data
     */
    private void saveProfilePicBitmapInInternalStorage(Bitmap bitmap) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(Constants.PROFILE_PIC_BITMAP_FILE, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            bitmap = null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onBackPressed() {
        boolean result = deleteSavedProfilePic();
        super.onBackPressed();
    }

    /**
     * if user presses back button, delete any pic saved in internal storage
     */
    private boolean deleteSavedProfilePic() {
        String profilePicStoragePath = getFilesDir() + "/" + Constants.PROFILE_PIC_BITMAP_FILE;
        if (TextUtils.isEmpty(profilePicStoragePath)) {
            return false;
        } else {
            File file = new File(profilePicStoragePath);
            return file.delete();
        }
    }

    /**
     * save user data in shared preferences
     * this data can be of old user or a new user
     *
     * @param userData user data
     */
    private void saveUserDataInPrefs(UserData userData) {

        mEditor.putLong(Constants.USER_ID, userData.getId());
        mEditor.putString(Constants.PROFILE_PIC, userData.getUserPic());
        mEditor.putString(Constants.NAME, userData.getUserName());
        mEditor.putString(Constants.EMAIL, userData.getEmail());
        mEditor.putString(Constants.PHONE_NUMBER, userData.getPhone());
        mEditor.putString(Constants.ADDRESS, userData.getAddress());
        mEditor.putString(Constants.LAT, String.valueOf(userData.getLat()));
        mEditor.putString(Constants.LON, String.valueOf(userData.getLon()));
        mEditor.putBoolean(Constants.IS_LOGIN, true);
        mEditor.apply();
        Intent i = new Intent(activity, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

}
