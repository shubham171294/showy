package com.showy.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.showy.R;
import com.showy.databinding.ActivityHomeBinding;
import com.showy.fragments.AboutUsFragment;
import com.showy.fragments.ContactUsFragment;
import com.showy.fragments.DesignFragment;
import com.showy.fragments.FavouriteFragment;
import com.showy.fragments.SharePicFragment;
import com.showy.models.User;
import com.showy.utils.ApiEndPointInterface;
import com.showy.utils.Constants;
import com.showy.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private ActivityHomeBinding binding;
    private ActionBarDrawerToggle drawerToggle;
    private Fragment fragment = null;
    private Class fragmentClass;
    private Activity activity;
    private SharedPreferences mSharedPreferences;
    private View headerLayout;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        init();

        binding.flContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        setProfilePicAndUserName();
    }

    private void setProfilePicAndUserName() {
        ImageView mUserImage = (ImageView) headerLayout.findViewById(R.id.iv_user_image);
        TextView mUserName = (TextView) headerLayout.findViewById(R.id.tv_username);
        TextView mUserPhoneNumber = (TextView) headerLayout.findViewById(R.id.tv_user_phone);

        mUserName.setText(mSharedPreferences.getString(Constants.NAME, ""));
        mUserPhoneNumber.setText(mSharedPreferences.getString(Constants.PHONE_NUMBER, ""));

        //read internal storage
        String profilePicStoragePath = getFilesDir() + "/" + Constants.PROFILE_PIC_BITMAP_FILE;
        File file = new File(profilePicStoragePath);
        try {
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
            if (b != null) {
                mUserImage.setImageBitmap(b);
                b = null;
                file = null;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            if (TextUtils.isEmpty(mSharedPreferences.getString(Constants.PROFILE_PIC, ""))) {
                Glide.with(activity).load(R.drawable.profile_icon).centerCrop().into(mUserImage);
            } else {
                Glide.with(activity).load(mSharedPreferences.getString(Constants.PROFILE_PIC, "")).asBitmap().centerCrop().into(new BitmapImageViewTarget(mUserImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        saveProfilePicBitmapInInternalStorage(resource);
                    }
                });
            }

        }


    }

    /**
     * saves user profile pic as a bitmap in internal storage
     * this storage doesn't require any permissions as it is private to application data
     *
     * @param bitmap
     */
    private void saveProfilePicBitmapInInternalStorage(Bitmap bitmap) {
        try {
            FileOutputStream fos = null;
            fos = openFileOutput(Constants.PROFILE_PIC_BITMAP_FILE, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void init() {

        activity = HomeActivity.this;
        binding.includeToolbar.toolbar.setTitle(getString(R.string.Design));
        // Setup drawer view
        setupDrawerContent(binding.nvView);
        drawerToggle = setupDrawerToggle();
        binding.drawerLayout.addDrawerListener(drawerToggle);

        mSharedPreferences = Utils.getSharedPrefs(getApplicationContext());
        fragmentClass = DesignFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }


    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.menu_item_design:
                                binding.drawerLayout.closeDrawers();
                                break;
                            case R.id.menu_item_match_color:
                                startActivity(new Intent(activity, MatchYourNailActivity.class));
                                break;
                            case R.id.menu_item_fav:
                                startActivity(new Intent(activity, FavouriteActivity.class));
                                break;
                            case R.id.menu_item_share_pic:
                                startActivity(new Intent(activity, SharePicActivity.class));
                                break;
                            case R.id.menu_item_contact:
                                startActivity(new Intent(activity, ContactUsActivity.class));
                                break;
                            case R.id.menu_item_about:
                                startActivity(new Intent(activity, AboutUsActivity.class));
                                break;
                            case R.id.menu_item_rate:
                                Utils.openBrowser(activity, getString(R.string.website));
                                break;
                            case R.id.menu_item_logout:
                                createLogoutDialog();
                                break;
                        }
                        return true;
                    }
                });
        headerLayout = navigationView.getHeaderView(0);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, binding.drawerLayout, binding.includeToolbar.toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        switch (menuItem.getItemId()) {
            case R.id.menu_item_design:
                fragmentClass = DesignFragment.class;
                break;
            case R.id.menu_item_fav:
                fragmentClass = FavouriteFragment.class;
                break;
            case R.id.menu_item_share_pic:
                fragmentClass = SharePicFragment.class;
                break;
            case R.id.menu_item_contact:
                fragmentClass = ContactUsFragment.class;
                break;
            case R.id.menu_item_about:
                fragmentClass = AboutUsFragment.class;
                break;
            case R.id.menu_item_rate:
                Utils.openBrowser(activity, getString(R.string.website));
                break;
            case R.id.menu_item_logout:
                createLogoutDialog();
                break;
            default:
                fragmentClass = DesignFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        binding.includeToolbar.toolbar.setTitle(menuItem.getTitle());
        // Close the navigation drawer
        binding.drawerLayout.closeDrawers();
    }

    private void createLogoutDialog() {
        new AlertDialog.Builder(activity)
                .setTitle("Sure to Logout ?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (mSharedPreferences.getBoolean(Constants.IS_LOGIN, false)) {
                            dialogInterface.dismiss();
                            handleLogout();
                        } else {
                            //this case won't occur, but still
                            Utils.showToastMessage(activity, "Please login first");
                        }

                    }
                })
                .setNeutralButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }

    private void handleLogout() {
        Utils.showDialog(activity, getString(R.string.loading));
        ApiEndPointInterface apiEndPointInterface = ApiEndPointInterface.retrofit.create(ApiEndPointInterface.class);
        Call<User> logoutUserCall = apiEndPointInterface.logout(mSharedPreferences.getLong(Constants.USER_ID, 0));
        logoutUserCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Utils.hideDialog();
                if (response.isSuccessful())
                    if (response.body().getStatus().trim().equalsIgnoreCase("success")) {
                        Utils.clearApplicationData(activity);
                        mSharedPreferences.edit().clear().apply();
                        Intent i = new Intent(activity, SplashScreenActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    } else {
                        if (response.body().getReason() != null && !response.body().getReason().trim().equalsIgnoreCase("null"))
                            Utils.showToastMessage(activity, response.body().getReason());
                        else
                            Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
                    }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Utils.hideDialog();
                Utils.showToastMessage(activity, getString(R.string.some_error_occurred));
                t.printStackTrace();
            }
        });
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                binding.drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Utils.showToastMessage(this, "Please click BACK again to exit");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
